import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { connect } from 'react-redux';
import ListItem from './ListItem';

class FriendList extends Component {
    
    renderItem(friend) {
        return <ListItem friend={friend.item} index={friend.index} />;
    }

    render() {
        console.log(this.props);
        return (
            <FlatList     
                data={this.props.friendsList}
                renderItem={this.renderItem}
                keyExtractor={(friend) => friend.name}
            />
        );
    }
}

const mapStateToProps = state => {
    return {
        friendsList: state.friends
    };
};

export default connect(mapStateToProps)(FriendList);
