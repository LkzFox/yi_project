import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import appStyle from '../styles/default';

const Button = (props) => {
    const { containerStyle } = styles;

    return (
        <TouchableOpacity 
            style={containerStyle} 
            onPress={props.onPress}
        >
            <Text style={buttonStyle(props.following)}> 
                {props.children}
            </Text>
        </TouchableOpacity>
    );
};

const styles = {
    containerStyle: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    defaultStyle: {
        backgroundColor: appStyle.primaryColor,
        color: '#FFF',
        padding: 3,
        paddingRight: 10,
        paddingLeft: 10,
        flexDirection: 'row',
        textAlign: 'center',
        minWidth: 80,
    }
};

const buttonStyle = (following) => {
    return {
        ...styles.defaultStyle,
        backgroundColor: following ? appStyle.secondaryColor : appStyle.primaryColor
    };
};

export default Button;
