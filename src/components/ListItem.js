import React, { Component } from 'react';
import { Image, Text, View } from 'react-native';
import { connect } from 'react-redux';
import Button from './Button';
import appStyle from '../styles/default';
import * as actions from '../actions';

class ListItem extends Component {

    renderButton() {
        if (this.props.following) {
            return (
                <Button
                    onPress={() => this.props.unfollow(this.props.friend.name)}
                    following
                >
                    SEGUINDO
                </Button>
            );
        }

        return (
            <Button
                onPress={() => this.props.follow(this.props.friend.name)}
            >
                SEGUIR
            </Button>
        );
    }

    render() {
        const { nameStyle } = styles;
        console.log(this.props);
        return (
            <View>
                <View style={chooseStyle(this.props.index + 1)}>
                    <Image 
                        style={{ height: 50, width: 50 }}
                        source={{ uri: 'https://loremflickr.com/320/240/brazil,girl/all' }} 
                    />
                    <Text style={nameStyle}>
                        {this.props.friend.name}
                    </Text>
                    {this.renderButton()}
                </View>
            </View>
        );
    }
}

const styles = {
    sectionStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        horizontalAlign: 'center',
        padding: 15,
        borderBottomWidth: 1,
        borderColor: '#aaa'
    },
    nameStyle: {
        flex: 2,
        marginLeft: 10,
        fontSize: 18
    }
};

const chooseStyle = index => {
    if (index % 2 === 0) {
        return {
            ...styles.sectionStyle,
            backgroundColor: '#fff'
        };
    }

    return {
        ...styles.sectionStyle,
        backgroundColor: `${appStyle.primaryColor}1a`
    };
};

const mapStateToProps = (state, ownProps) => {
    const following = state.following.includes(ownProps.friend.name);
    return { following };
};

export default connect(mapStateToProps, actions)(ListItem);
