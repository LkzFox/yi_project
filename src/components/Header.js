import React from 'react';
import { Text, View } from 'react-native';
import appStyle from '../styles/default';
import Button from './Button';

const Header = () => {
    const { containerStyle, titleStyle, textStyle, buttonContainerStyle } = styles;
    return (
        <View style={containerStyle}>
            <View style={{ flex: 2 }}>
                <Text style={titleStyle}>
                    EXCELENTE!
                </Text>
                <Text style={textStyle}>
                    Para aproveitar ao máximo, conecte-se com seus amigos!
                </Text>
            </View>
            <View style={buttonContainerStyle}>
                <Button>
                    CONTINUAR
                </Button>
            </View>
        </View>
    );
};

const styles = {
    containerStyle: {
        borderWidth: 5,
        borderColor: appStyle.primaryColor,
        flexDirection: 'row'
    },
    titleStyle: {
        borderLeftWidth: 5,
        borderColor: appStyle.secondaryColor,
        color: appStyle.secondaryColor,
        fontSize: 30,
        fontWeight: '600',
        paddingLeft: 15,
        marginTop: 10,
        marginLeft: -5,
    },
    textStyle: {
        color: appStyle.primaryColor,
        fontSize: 18,
        fontWeight: '600',
        padding: 5,
        paddingLeft: 15,
        fontFamily: 'Arial'
    },
    buttonContainerStyle: { 
        flex: 1, 
        alignSelf: 'flex-end', 
        padding: 5, 
        alignItems: 'flex-end' 
    }
};


export default Header;
