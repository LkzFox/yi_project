import React from 'react';
import { View } from 'react-native';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import Header from './components/Header';
import FriendList from './components/FriendList';
import reducers from './reducers';

const App = () => (
    <Provider store={createStore(reducers)}>
        <View style={{ padding: 5 }}>
            <Header />
        </View>
        <FriendList />
    </Provider>
);

export default App;
