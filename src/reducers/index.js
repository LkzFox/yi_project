import { combineReducers } from 'redux';
import FriendsReducer from './FriendsReducer';
import FollowingReducer from './FollowingReducer';

export default combineReducers({
    friends: FriendsReducer,
    following: FollowingReducer
});
