export default (state = [], action) => {
    switch (action.type) {
        case 'FOLLOW':
            return state.concat(action.payload);
        case 'UNFOLLOW':
            return state.filter(element => element !== action.payload);
        default:
            return state;
    }
};
