export const follow = (name) => {
    return {
        type: 'FOLLOW',
        payload: name
    };
};

export const unfollow = (name) => {
    return {
        type: 'UNFOLLOW',
        payload: name
    };
};
